<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\GradeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\Kabag\DashboardController as KabagDashboardController;
use App\Http\Controllers\Kabag\GuestController as KabagGuestController;
use App\Http\Controllers\Kabag\LetterController as KabagLetterController;
use App\Http\Controllers\Kabalai\DashboardController as KabalaiDashboardController;
use App\Http\Controllers\Kabalai\GuestController as KabalaiGuestController;
use App\Http\Controllers\Kabalai\LetterController as KabalaiLetterController;
use App\Http\Controllers\Staff\DashboardController as StaffDashboardController;
use App\Http\Controllers\Staff\GuestController as StaffGuestController;
use App\Http\Controllers\Staff\LetterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GuestController::class, 'index'])->name('guest');
Route::post('/store', [GuestController::class, 'store'])->name('guest.store');
Route::get('/reload-captcha', [GuestController::class, 'reloadCaptcha']);

// Auth
Route::get('/auth', [AuthController::class, 'index'])->name('auth');
Route::post('/auth', [AuthController::class, 'authenticate'])->name('authenticate');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Route::group(['prefix' => 'admin', 'middleware' => ['auth.middleware']], function () {
    Route::redirect('/', '/admin/dashboard');

    // Dashboard
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    // Users
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'index'])->name('admin.users');
        Route::post('/store', [UserController::class, 'store'])->name('admin.users.store');
        Route::get('/edit/{id}', [UserController::class, 'edit']);
        Route::put('/update/{id}', [UserController::class, 'update']);
        Route::delete('/destroy/{id}', [UserController::class, 'destroy']);
    });

    // Grade
    Route::group(['prefix' => 'grades'], function () {
        Route::get('/', [GradeController::class, 'index'])->name('admin.grades');
        Route::post('/store', [GradeController::class, 'store'])->name('admin.grades.store');
        Route::get('/edit/{id}', [GradeController::class, 'edit']);
        Route::put('/update/{id}', [GradeController::class, 'update']);
        Route::delete('/destroy/{id}', [GradeController::class, 'destroy']);
    });
});

Route::group(['prefix' => 'kabalai', 'middleware' => ['auth.middleware']], function () {
    Route::redirect('/', '/kabalai/dashboard');

    // Dashboard
    Route::get('/dashboard', [KabalaiDashboardController::class, 'index'])->name('kabalai.dashboard');

    // Guest
    Route::group(['prefix' => 'guest'], function () {
        Route::get('/', [KabalaiGuestController::class, 'index'])->name('kabalai.guest');
        Route::get('/print', [KabalaiGuestController::class, 'print'])->name('kabalai.guest.print');
    });

    // Letter
    Route::group(['prefix' => 'letter'], function () {
        Route::get('/', [KabalaiLetterController::class, 'index'])->name('kabalai.letter');
        Route::put('/update/{id}', [KabalaiLetterController::class, 'update']);
        Route::get('/print', [KabalaiLetterController::class, 'print'])->name('kabalai.letter.print');
    });
});

Route::group(['prefix' => 'kabag', 'middleware' => ['auth.middleware']], function () {
    Route::redirect('/', '/kabag/dashboard');

    // Dashboard
    Route::get('/dashboard', [KabagDashboardController::class, 'index'])->name('kabag.dashboard');

    // Guest
    Route::group(['prefix' => 'guest'], function () {
        Route::get('/', [KabagGuestController::class, 'index'])->name('kabag.guest');
    });

    // Letter
    Route::group(['prefix' => 'letter'], function () {
        Route::get('/', [KabagLetterController::class, 'index'])->name('kabag.letter');
        Route::get('/detail/{id}', [KabagLetterController::class, 'detail']);
    });
});

Route::group(['prefix' => 'staff', 'middleware' => ['auth.middleware']], function () {
    Route::redirect('/', '/staff/dashboard');

    // Dashboard
    Route::get('/dashboard', [StaffDashboardController::class, 'index'])->name('staff.dashboard');

    // Guest
    Route::group(['prefix' => 'guest'], function () {
        Route::get('/', [StaffGuestController::class, 'index'])->name('staff.guest');
        Route::get('/print', [StaffGuestController::class, 'print'])->name('staff.guest.print');
    });

    // Letter
    Route::group(['prefix' => 'letter'], function () {
        Route::get('/', [LetterController::class, 'index'])->name('staff.letter');
        Route::get('/create', [LetterController::class, 'create'])->name('staff.letter.create');
        Route::post('/store', [LetterController::class, 'store'])->name('staff.letter.store');
        Route::delete('/destroy/{id}', [LetterController::class, 'destroy']);
        Route::get('/print', [LetterController::class, 'print'])->name('staff.letter.print');
    });
});
