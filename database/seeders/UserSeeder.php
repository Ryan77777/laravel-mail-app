<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'grade_id' => 1,
                'name' => 'Superadmin',
                'email' => 'admin@mail.com',
                'password' => Hash::make('12345'),
                'employment_number' => '41173389749',
                'position' => 'Admin',
                'role' => 'admin',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
