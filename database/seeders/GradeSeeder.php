<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grades')->insert([
            [
                'name' => 'Kabag 1',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Kabag 2',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
