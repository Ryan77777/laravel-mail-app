<?php

namespace App\Http\Controllers\Kabalai;

use App\Http\Controllers\Controller;
use App\Models\Letter;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;


class LetterController extends Controller
{

    protected $customMessages = [
        'note.required' => 'Catatan harus diisi',
        'user_id.required' => 'Silahkan pilih Kelapa Bagian terlebih dahulu',
    ];

    public function index(Request $request)
    {
        $user = Auth::user();

        $from = $request->from_date;
        $to = $request->to_date;

        if ($user->role === 'kabalai') {
            if (request()->ajax()) {
                if (!empty($request->from_date)) {
                    $data = Letter::whereBetween('date', array($from, $to))
                        ->get();
                } else {
                    $data = Letter::get();
                }
                return DataTables::of($data)->addIndexColumn()
                    ->addColumn('document', 'pages.kabalai.letter.document')
                    ->addColumn('action', 'pages.kabalai.letter.action')
                    ->rawColumns(['document', 'action'])
                    ->make(true);
            }
            $grades  = User::where('role', 'kabag')->get();
            return view('pages.kabalai.letter.index', compact('grades'));
        }
        return back();
    }

    public function update($id)
    {
        request()->validate([
            'note'              => 'required|string',
            'user_id'           => 'required',
        ], $this->customMessages);

        $data = Letter::findOrFail($id);

        $data->update([
            'note'              => strip_tags(request()->post('note')),
            'user_id'           => request()->post('user_id'),
        ]);

        return response()->json($data);
    }

    public function print(Request $request)
    {
        $user = Auth::user();

        $from = $request->from_date;
        $to = $request->to_date;


        if ($user->role === 'kabalai') {
            if (!empty($request->from_date)) {
                $letters = Letter::whereBetween('date', array($from, $to))
                    ->get();
            } else {
                $letters = Letter::get();
            }
            $data = [
                'title'     => 'Laporan Surat Masuk',
                'letters'   => $letters
            ];

            $pdf = PDF::loadView('print.letter', $data)->setPaper('a4', 'portrait');


            $path = public_path('pdf/');
            $fileName =  time() . '.' . 'pdf';
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/' . $fileName);
            return response()->download($pdf);
        }
        return back();
    }
}
