<?php

namespace App\Http\Controllers\Kabag;

use App\Http\Controllers\Controller;
use App\Models\Letter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class LetterController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();

        $from = $request->from_date;
        $to = $request->to_date;

        if ($user->role === 'kabag') {
            if (request()->ajax()) {
                if (!empty($request->from_date)) {
                    $data = Letter::whereBetween('date', array($from, $to))->where('user_id', $user->id)
                        ->get();
                } else {
                    $data = Letter::where('user_id', $user->id)->get();
                }
                return DataTables::of($data)->addIndexColumn()
                    ->addColumn('document', 'pages.kabag.letter.document')
                    ->addColumn('action', 'pages.kabag.letter.action')
                    ->rawColumns(['document', 'action'])
                    ->make(true);
            }
            return view('pages.kabag.letter.index');
        }
        return back();
    }

    public function detail($id)
    {
        $user = Auth::user();

        if ($user->role === 'kabag') {
            $data = Letter::findOrFail($id);

            return response()->json($data);
        }
        return back();
    }
}
