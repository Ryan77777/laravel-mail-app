<?php

namespace App\Http\Controllers\Kabag;

use App\Http\Controllers\Controller;
use App\Models\Guest;
use App\Models\Letter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();

        $letter = Letter::count();
        $guest = Guest::count();

        if ($user->role === 'kabag') {
            return view('pages.kabag.dashboard.index', compact('letter', 'guest'));
        }
        return back();
    }
}
