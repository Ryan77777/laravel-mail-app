<?php

namespace App\Http\Controllers\Kabag;

use App\Http\Controllers\Controller;
use App\Models\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class GuestController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();

        $from = $request->from_date;
        $to = $request->to_date;

        if ($user->role === 'kabag') {
            if (request()->ajax()) {
                if (!empty($request->from_date)) {
                    $data = Guest::whereBetween('date', array($from, $to))
                        ->get();
                } else {
                    $data = Guest::get();
                }
                return DataTables::of($data)->addIndexColumn()
                    ->make(true);
            }
            return view('pages.kabag.guest.index');
        }
        return back();
    }
}
