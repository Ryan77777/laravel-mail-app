<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Models\Letter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\File;
use Barryvdh\DomPDF\Facade\Pdf;

class LetterController extends Controller
{
    protected $customMessages = [
        'letter_number.required' => 'Nomor surat masuk harus diisi',
        'sender.required' => 'Asal instansi harus diisi',
        'date.required' => 'Tanggal surat masuk harus diisi',
        'document.required' => 'File surat masuk harus diisi',
        'description.required' => 'Keterangan harus diisi',
        'document.uploaded' => 'Maksimal size 2MB',
    ];

    public function index(Request $request)
    {
        $user = Auth::user();

        $from = $request->from_date;
        $to = $request->to_date;

        if ($user->role === 'staff') {
            if (request()->ajax()) {
                if (!empty($request->from_date)) {
                    $data = Letter::whereBetween('date', array($from, $to))
                        ->get();
                } else {
                    $data = Letter::get();
                }
                return DataTables::of($data)->addIndexColumn()
                    ->addColumn('document', 'pages.staff.letter.document')
                    ->addColumn('action', 'pages.staff.letter.action')
                    ->rawColumns(['document', 'action'])
                    ->make(true);
            }
            return view('pages.staff.letter.index');
        }
        return back();
    }

    public function create()
    {
        return view('pages.staff.letter.create');
    }

    public function store(Request $request)
    {
        request()->validate([
            'letter_number'         => 'required|string',
            'sender'                => 'required|string',
            'date'                  => 'required|date',
            "document"              => "required|mimes:pdf|max:2048",
            'description'           => 'required|string',
        ], $this->customMessages);

        $data = new Letter();
        $data->letter_number            = strip_tags(request()->post('letter_number'));
        $data->sender                   = strip_tags(request()->post('sender'));
        $data->date                     = request()->post('date');
        $data->description              = strip_tags(request()->post('description'));

        if ($request->hasfile('document')) {
            $file = $request->file('document');
            $name = time() . rand(1, 100) . '.' . $file->extension();
            if ($file->move(public_path('uploads'), $name)) {
                $files[] = $name;
                $data['document'] = $name;
            }
        }

        $data->save();

        return redirect()->route('staff.letter')->with('success', "Data berhasil ditambahkan!");
    }

    public function destroy($id)
    {
        $item = Letter::findOrFail($id);

        if ($item->document) {
            $fileName2 = public_path() . '/uploads/' . $item->document;
            File::delete($fileName2);
        }

        $itemDelete = $item->delete();

        return response()->json($itemDelete);
    }

    public function print(Request $request)
    {
        $user = Auth::user();

        $from = $request->from_date;
        $to = $request->to_date;


        if ($user->role === 'staff') {
            if (!empty($request->from_date)) {
                $letters = Letter::whereBetween('date', array($from, $to))
                    ->get();
            } else {
                $letters = Letter::get();
            }
            $data = [
                'title'     => 'Laporan Surat Masuk',
                'letters'   => $letters
            ];

            $pdf = PDF::loadView('print.letter', $data)->setPaper('a4', 'portrait');


            $path = public_path('pdf/');
            $fileName =  time() . '.' . 'pdf';
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/' . $fileName);
            return response()->download($pdf);
        }
        return back();
    }
}
