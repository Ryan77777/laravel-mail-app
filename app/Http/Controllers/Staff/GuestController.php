<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\Controller;
use App\Models\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Barryvdh\DomPDF\Facade\Pdf;

class GuestController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();

        $from = $request->from_date;
        $to = $request->to_date;

        if ($user->role === 'staff') {
            if (request()->ajax()) {
                if (!empty($request->from_date)) {
                    $data = Guest::whereBetween('date', array($from, $to))
                        ->get();
                } else {
                    $data = Guest::get();
                }
                return DataTables::of($data)->addIndexColumn()
                    ->make(true);
            }
            return view('pages.staff.guest.index');
        }
        return back();
    }

    public function print(Request $request)
    {
        $user = Auth::user();

        $from = $request->from_date;
        $to = $request->to_date;


        if ($user->role === 'staff') {
            if (!empty($request->from_date)) {
                $guests = Guest::whereBetween('date', array($from, $to))
                    ->get();
            } else {
                $guests = Guest::get();
            }
            $data = [
                'title'     => 'Laporan Buku Tamu',
                'guests'    => $guests
            ];

            $pdf = PDF::loadView('print.guest', $data)->setPaper('a4', 'portrait');


            $path = public_path('pdf/');
            $fileName =  time() . '.' . 'pdf';
            $pdf->save($path . '/' . $fileName);

            $pdf = public_path('pdf/' . $fileName);
            return response()->download($pdf);
        }
        return back();
    }
}
