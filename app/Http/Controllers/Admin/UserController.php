<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $customMessages = [
        'employment_number.required' => 'NIP harus diisi',
        'name.required' => 'Nama lengkap harus diisi',
        'position.required' => 'Nama jabatan harus diisi',
        'email.required' => 'Email harus diisi',
        'password.required' => 'Password harus diisi',
        'grade_id.required' => 'Silahkan pilih Pangkat / Golongan terlebih dahulu',
        'role.required' => 'Silahkan pilih Role terlebih dahulu',
    ];

    public function index(Request $request)
    {
        $user = Auth::user();

        if ($user->role === 'admin') {
            if ($request->ajax()) {
                $data = User::with('grade')->whereNotIn('id', [1]);
                return Datatables::of($data)->addIndexColumn()
                    ->addColumn('role', 'pages.admin.users.role')
                    ->addColumn('grade', 'pages.admin.users.grade')
                    ->addColumn('action', 'pages.admin.users.action')
                    ->rawColumns(['role', 'grade', 'action'])
                    ->make(true);
            }

            $grades  = Grade::get();
            return view('pages.admin.users.index', compact('grades'));
        }
        return back();
    }

    public function store(Request $request)
    {
        $request->validate([
            'grade_id'          => 'required',
            'name'              => 'required|string',
            'email'             => 'required|email',
            'password'          => 'required|string',
            'employment_number' => 'required|string',
            'position'          => 'required|string',
            'role'              => 'required',
        ], $this->customMessages);

        $data = User::create([
            'grade_id'          => request()->post('grade_id'),
            'name'              => strip_tags(request()->post('name')),
            'email'             => strip_tags(request()->post('email')),
            'password'          => Hash::make(request()->post('password')),
            'employment_number' => strip_tags(request()->post('employment_number')),
            'position'          => strip_tags(request()->post('position')),
            'role'              => strip_tags(request()->post('role')),
        ]);

        return response()->json($data);
    }

    public function edit($id)
    {
        $user = Auth::user();

        if ($user->role === 'admin') {
            $data = User::findOrFail($id);

            return response()->json($data);
        }
        return back();
    }

    public function update($id)
    {
        request()->validate([
            'grade_id'          => 'required',
            'name'              => 'required|string',
            'email'             => 'required|email',
            'password'          => 'nullable|string',
            'employment_number' => 'required|string',
            'position'          => 'required|string',
            'role'              => 'required',
        ], $this->customMessages);

        $data = User::findOrFail($id);

        if (request()->post('password') == '') {
            $data->update([
                'grade_id'          => request()->post('grade_id'),
                'name'              => strip_tags(request()->post('name')),
                'email'             => strip_tags(request()->post('email')),
                'employment_number' => strip_tags(request()->post('employment_number')),
                'position'          => strip_tags(request()->post('position')),
                'role'              => strip_tags(request()->post('role')),
            ]);
        } else {
            $data->update([
                'grade_id'          => request()->post('grade_id'),
                'name'              => strip_tags(request()->post('name')),
                'email'             => strip_tags(request()->post('email')),
                'password'          => Hash::make(request()->post('password')),
                'employment_number' => strip_tags(request()->post('employment_number')),
                'position'          => strip_tags(request()->post('position')),
                'role'              => strip_tags(request()->post('role')),
            ]);
        }

        return response()->json($data);
    }

    public function destroy($id)
    {
        $data = User::destroy($id);

        return response()->json($data);
    }
}
