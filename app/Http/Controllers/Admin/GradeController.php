<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class GradeController extends Controller
{
    protected $customMessages = [
        'required' => 'Nama pangkat harus diisi',
        'unique' => 'Nama Pangkat sudah tersedia.',
    ];

    public function index(Request $request)
    {
        $user = Auth::user();

        if ($user->role === 'admin') {
            if ($request->ajax()) {
                $data = Grade::get();
                return DataTables::of($data)->addIndexColumn()
                    ->addColumn('action', 'pages.admin.grade.action')
                    ->rawColumns(['action'])
                    ->make(true);
            }

            return view('pages.admin.grade.index');
        }
        return back();
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
        ], $this->customMessages);


        $data = Grade::create([
            'name' => strip_tags(request()->post('name')),
        ]);

        return response()->json($data);
    }

    public function edit($id)
    {
        $user = Auth::user();

        if ($user->role === 'admin') {
            $data = Grade::findOrFail($id);

            return response()->json($data);
        }
        return back();
    }

    public function update(Request $request, $id)
    {
        $data = Grade::findOrFail($id);

        request()->validate([
            'name' => "required|string|unique:grades,name,{$data->name},name|max:50",
        ], $this->customMessages);

        $data->update([
            'name' => strip_tags(request()->post('name')),
        ]);

        return response()->json($data);
    }

    public function destroy($id)
    {
        $data = Grade::destroy($id);

        return response()->json($data);
    }
}
