<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();

        $kabalai = User::where('role', 'kabalai')->count();
        $kabag = User::where('role', 'kabag')->count();
        $staff = User::where('role', 'staff')->count();

        if ($user->role === 'admin') {
            return view('pages.admin.dashboard.index', compact('kabalai', 'kabag', 'staff'));
        }
        return back();
    }
}
