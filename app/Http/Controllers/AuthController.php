<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if ($user) {
            if ($user->role == 'admin') {
                return redirect()->route('admin.dashboard');
            } else if ($user->role == 'kabalai') {
                return redirect()->route('kabalai.dashboard');
            } else if ($user->role == 'kabag') {
                return redirect()->route('kabag.dashboard');
            } else if ($user->role == 'staff') {
                return redirect()->route('staff.dashboard');
            }
        }
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            if (Auth::user()->role == 'admin') {
                return redirect()->route('admin.dashboard');
            }

            if (Auth::user()->role == 'kabalai') {
                return redirect()->route('kabalai.dashboard');
            }

            if (Auth::user()->role == 'kabag') {
                return redirect()->route('kabag.dashboard');
            }

            if (Auth::user()->role == 'staff') {
                return redirect()->route('staff.dashboard');
            }
        }

        return back()->withErrors([
            'error' => 'Email atau password salah'
        ])->withInput();
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('auth');
    }
}
