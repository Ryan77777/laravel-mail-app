<?php

namespace App\Http\Controllers;

use App\Models\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GuestController extends Controller
{
    protected $customMessages = [
        'name.required' => 'Nama lengkap tidak boleh kosong',
        'agency.required' => 'Asal instansi tidak boleh kosong',
        'phone.required' => 'Nomor telepon tidak boleh kosong',
        'email.required' => 'Email tidak boleh kosong',
        'date.required' => 'Tanggal tidak boleh kosong',
        'description.required' => 'Keterangan tidak boleh kosong',
        'g-recaptcha-response.required' => 'Please verify that you are not a robot.',
    ];
    public function index()
    {
        return view('pages.guest');
    }

    public function store(Request $request)
    {

        $rules = [
            'name'              => 'required|string',
            'agency'            => 'required|string',
            'phone'             => 'required|string',
            'email'             => 'required|email',
            'date'              => 'required|date',
            'description'       => 'required|string',
            'g-recaptcha-response'   => 'required|captcha'
        ];

        $validator = Validator::make($request->all(), $rules, $this->customMessages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        Guest::create([
            'name'              => strip_tags(request()->post('name')),
            'agency'            => strip_tags(request()->post('agency')),
            'phone'             => strip_tags(request()->post('phone')),
            'email'             => strip_tags(request()->post('email')),
            'date'              => request()->post('date'),
            'description'       => strip_tags(request()->post('description')),
        ]);

        return back()
            ->with('success', 'Data berhasil dikirim!');
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha' => captcha_img()]);
    }
}
