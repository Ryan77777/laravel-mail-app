<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">Stisla</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="#">St</a>
        </div>
        <ul class="sidebar-menu">
            @if (Auth::user()->role === 'admin')
            <li class="menu-header">Dashboard</li>
            <li class="{{ Request::is('admin/dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/admin/dashboard') }}"><i class="fa-solid fa-chart-simple"></i> <span>Dashboard</span></a>
            </li>
            <li class="menu-header">Master Data</li>
            <li class="{{ Request::is('admin/users') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('admin/users') }}">
                    <i class="fa-solid fa-users"></i> <span>Data User</span>
                </a>
            </li>
            <li class="{{ Request::is('admin/grades') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('admin/grades') }}">
                    <i class="fa-solid fa-graduation-cap"></i> <span>Data Pangkat</span>
                </a>
            </li>
            @endif
            @if (Auth::user()->role === 'kabalai')
            <li class="menu-header">Dashboard</li>
            <li class="{{ Request::is('kabalai/dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/kabalai/dashboard') }}"><i class="fa-solid fa-chart-simple"></i> <span>Dashboard</span></a>
            </li>
            <li class="menu-header">Master Data</li>
            <li class="{{ Request::is('kabalai/guest') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('kabalai/guest') }}">
                    <i class="fa-solid fa-book"></i> <span>Buku Tamu</span>
                </a>
            </li>
            <li class="{{ Request::is('kabalai/letter') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('kabalai/letter') }}">
                    <i class="fa-solid fa-inbox"></i> <span>Surat Masuk</span>
                </a>
            </li>
            @endif
            @if (Auth::user()->role === 'kabag')
            <li class="menu-header">Dashboard</li>
            <li class="{{ Request::is('kabag/dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/kabag/dashboard') }}"><i class="fa-solid fa-chart-simple"></i> <span>Dashboard</span></a>
            </li>
            <li class="menu-header">Master Data</li>
            <li class="{{ Request::is('kabag/guest') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('kabag/guest') }}">
                    <i class="fa-solid fa-book"></i> <span>Buku Tamu</span>
                </a>
            </li>
            <li class="{{ Request::is('kabag/letter') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('kabag/letter') }}">
                    <i class="fa-solid fa-inbox"></i> <span>Surat Masuk</span>
                </a>
            </li>
            @endif
            @if (Auth::user()->role === 'staff')
            <li class="menu-header">Dashboard</li>
            <li class="{{ Request::is('staff/dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('/staff/dashboard') }}"><i class="fa-solid fa-chart-simple"></i> <span>Dashboard</span></a>
            </li>
            <li class="menu-header">Master Data</li>
            <li class="{{ Request::is('staff/guest') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('staff/guest') }}">
                    <i class="fa-solid fa-book"></i> <span>Buku Tamu</span>
                </a>
            </li>
            <li class="{{ Request::is('staff/letter') ? 'active' : '' }}">
                <a class="nav-link" href="{{ url('staff/letter') }}">
                    <i class="fa-solid fa-inbox"></i> <span>Surat Masuk</span>
                </a>
            </li>
            @endif
        </ul>

    </aside>
</div>