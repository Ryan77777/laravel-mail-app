@extends('layouts.app')

@section('title', 'Surat Masuk')

@push('style')
<!-- CSS Libraries -->
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.min.css">

@endpush

@section('main')

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="user-form">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="note">Keterangan <sup class="text-danger">*</sup></label>
                        <textarea placeholder="Catatan Disposisi" class="form-control form-control-sm @error('note') is-invalid @enderror" name="note" id="note" value="{{ old('description') }}"></textarea>
                        <div class="invalid-feedback" id="valid-note">{{ $errors->first('note') }}</div>
                    </div>
                    <div class="form-group">
                        <label for="user_id">Kepala Bagian</label>
                        <select class="select2 form-control form-control-sm @error('user_id') is-invalid @enderror" name="user_id" id="user_id">
                            <option value="" selected disabled>-- Pilih Kepala Bagian --</option>
                            @foreach ($grades as $grade)
                            <option value="{{ $grade->id }}" {{ old('user_id') == $grade->name ? 'selected' : '' }}>{{ $grade->name }}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback" id="valid-user_id">{{ $errors->first('user_id') }}</div>
                    </div>
                </form>

            </div>
            <div class="modal-footer no-bd">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fas fa-times"></i>
                    Close
                </button>
                <button type="button" id="btn-save" class="btn btn-primary">
                    <i class="fas fa-check"></i>
                    Kirim
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Main -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Surat Masuk</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Surat Masuk</div>
            </div>
        </div>

        @if (session('success'))
        <div class="alert alert-primary alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {!! session('success') !!}
            </div>
        </div>
        @endif

        <div class="section-body">
            <div class="card card-primary">
                <div class="card-header">
                    <div class="row">
                        <input type="date" class="form-control form-control-sm w-25 mr-2" name="from" id="from">
                        <input type="date" class="form-control form-control-sm w-25 mr-2" name="to" id="to">
                        <button class="btn btn-primary mr-2" id="btn-filter">
                            Filter
                        </button>
                        <button class="btn btn-danger" id="btn-reset">
                            Reset
                        </button>
                    </div>
                    <button class="btn btn-primary ml-auto" id="btn-print">
                        <i class="fas fa-print"></i>
                        Print
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped w-100" id="table-letter">
                            <thead>
                                <tr>
                                    <th width="30px">No</th>
                                    <th>No. Surat Masuk</th>
                                    <th>Asal Instansi</th>
                                    <th>Tanggal Masuk</th>
                                    <th>Keterangan</th>
                                    <th>Dokumen</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('scripts')
<!-- JS Libraies -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.all.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // Setup AJAX CSRF
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        load_data();

        function load_data(from_date = '', to_date = '') {
            $('#table-letter').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{ route('kabalai.letter') }}",
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'letter_number',
                        name: 'letter_number',
                    },
                    {
                        data: 'sender',
                        name: 'sender',
                    },
                    {
                        data: 'date',
                        name: 'date',
                    },
                    {
                        data: 'description',
                        name: 'description',
                    },
                    {
                        data: 'document',
                        name: 'document',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        className: 'text-center',
                        orderable: false,
                        searchable: false
                    },
                ],
            });
        }

        // Open Modal
        $('body').on('click', '#btn-send', function() {
            var id = $(this).val();

            $('#user-form').find('.form-control').removeClass('is-invalid is-valid');
            $('#id').val(id);
            $('#btn-save').val('update').removeAttr('disabled');
            $('#formModal').modal('show');
            $('.modal-title').html('Disposisikan Kepada: ');
            $('#btn-save').html('<i class="fas fa-check"></i> Kirim');
        });

        // Store new grade or update grade
        $('#btn-save').click(function() {
            var formData = {
                note: $('#note').val(),
                user_id: $('#user_id').val(),
            };
            var state = $('#btn-save').val();
            var type = "PUT";
            var id = $('#id').val();
            var ajaxurl = "{{ route('kabalai.letter') }}" + '/update/' + id;
            $('#btn-save').html('<i class="fas fa-cog fa-spin"></i> Sending...').attr("disabled", true);
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function(data) {
                    swal.fire("Berhasil!", 'Data berhasil di disposisikan', "success");
                    $('#table-letter').DataTable().draw(false);
                    $('#table-letter').DataTable().on('draw', function() {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                    $('#formModal').modal('hide');
                },
                error: function(data) {
                    try {
                        if (data.responseJSON.errors.note) {
                            $('#note').removeClass('is-valid').addClass('is-invalid');
                            $('#valid-note').removeClass('valid-feedback').addClass('invalid-feedback');
                            $('#valid-note').html(data.responseJSON.errors.note);
                        }
                        if (data.responseJSON.errors.user_id) {
                            $('#user_id').removeClass('is-valid').addClass('is-invalid');
                            $('#valid-user_id').removeClass('valid-feedback').addClass('invalid-feedback');
                            $('#valid-user_id').html(data.responseJSON.errors.user_id);
                        }
                        $('#btn-save').html('<i class="fas fa-check"></i> Kirim');
                        $('#btn-save').removeAttr('disabled');
                    } catch {
                        swal.fire("Maaf!", 'Terjadi kesalahan, Silahkan coba lagi', "error");
                        $('#formModal').modal('hide');
                    }
                }
            });
        });

        $('#btn-filter').click(function() {
            var from_date = $('#from').val();
            var to_date = $('#to').val();
            if (from_date != '' && to_date != '') {
                $('#table-letter').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#btn-reset').click(function() {
            $('#from').val('');
            $('#to').val('');
            $('#table-letter').DataTable().destroy();
            load_data();
        });

        $('#btn-print').click(function() {
            var formData = {
                from_date: $('#from').val(),
                to_date: $('#to').val(),
            };

            $.ajax({
                type: "GET",
                url: "{{ route('kabalai.letter.print') }}",
                data: formData,
                xhrFields: {
                    responseType: 'blob'
                },
                success: function(response) {
                    var blob = new Blob([response]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Laporan.pdf";
                    link.click();
                },
                error: function(blob) {
                    console.log(blob);
                }
            });
        });
    });
</script>
@endpush