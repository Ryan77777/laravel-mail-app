@if(!empty($user_id))
<div class="form-button-action">
    <div data-toggle="tooltip" id="btn-send" class="btn btn-sm btn-icon btn-secondary disabled" style="cursor: no-drop" value="{{ $id }}">
        Kirim disposisi
    </div>
</div>
@else
<div class="form-button-action">
    <button data-toggle="tooltip" id="btn-send" class="btn btn-sm btn-icon btn-primary" value="{{ $id }}">
        Kirim disposisi
    </button>
</div>
@endif