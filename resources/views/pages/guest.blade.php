@extends('layouts.auth')

@section('title', 'E-Office')

@push('style')
<!-- CSS Libraries -->
<link rel="stylesheet" href="{{ asset('library/bootstrap-social/bootstrap-social.css') }}">
<link rel="stylesheet" href="{{ asset('library/izitoast/dist/css/iziToast.min.css') }}">

@endpush

@section('main')
<div class="card card-primary">
    <div class="card-header">
        <h4>Form Buku Tamu</h4>
    </div>

    <div class="card-body">
        <form action="{{ route('guest.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nama Lengkap <sup class="text-danger">*</sup></label>
                <input id="name" type="text" value="{{ old('name') }}" placeholder="Masukkan nama lengkap..." class="form-control @error('name') is-invalid @enderror" name="name" autocomplete="off" tabindex="1" autofocus>
                @error('name')
                <small class="text-danger">
                    {{ $message }}
                </small>
                @enderror
            </div>

            <div class="form-group">
                <label for="agency">Asal Instansi <sup class="text-danger">*</sup></label>
                <input id="agency" type="text" value="{{ old('agency') }}" placeholder="Masukkan asal instansi..." class="form-control @error('agency') is-invalid @enderror" name="agency" autocomplete="off" tabindex="1">
                @error('agency')
                <small class="text-danger">
                    {{ $message }}
                </small>
                @enderror
            </div>

            <div class="form-group">
                <label for="phone">No Hp (Aktif & dapat dihubungi) <sup class="text-danger">*</sup></label>
                <input id="phone" type="text" value="{{ old('phone') }}" placeholder="Masukkan nomor telepon..." class="form-control @error('phone') is-invalid @enderror" name="phone" autocomplete="off" tabindex="1">
                @error('phone')
                <small class="text-danger">
                    {{ $message }}
                </small>
                @enderror
            </div>

            <div class="form-group">
                <label for="email">Email (Aktif) <sup class="text-danger">*</sup></label>
                <input id="email" type="email" value="{{ old('email') }}" placeholder="Masukkan email..." class="form-control @error('email') is-invalid @enderror" name="email" autocomplete="off" tabindex="1">
                @error('email')
                <small class="text-danger">
                    {{ $message }}
                </small>
                @enderror
            </div>

            <div class="form-group">
                <label for="date">Tanggal <sup class="text-danger">*</sup></label>
                <input type="date" value="{{ old('date') }}" class="form-control @error('date') is-invalid @enderror" name="date" id="date" value="<?= date('Y-m-d'); ?>">
                @error('date')
                <small class="text-danger">
                    {{ $message }}
                </small>
                @enderror
            </div>

            <div class="form-group">
                <label for="description">Keperluan <sup class="text-danger">*</sup></label>
                <textarea value="{{ old('description') }}" class="form-control @error('description') is-invalid @enderror" name="description" id="description"></textarea>
                @error('description')
                <small class="text-danger">
                    {{ $message }}
                </small>
                @enderror
            </div>

            <div class="form-group">
                {!! NoCaptcha::renderJs('en', false, 'onloadCallback') !!}
                {!! NoCaptcha::display() !!}
                @error('g-recaptcha-response')
                <small class="text-danger">
                    {{ $message }}
                </small>
                @enderror
            </div>

            <div class="form-group mt-4">
                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                    Simpan
                </button>
            </div>
        </form>

    </div>
</div>
@endsection

@push('scripts')
<!-- JS Libraies -->
<script src="{{ asset('library/izitoast/dist/js/iziToast.min.js') }}"></script>



<script type="text/javascript">
    $(document).ready(function() {
        // Setup AJAX CSRF
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        @if(Session::has('success'))
        iziToast.success({
            title: 'Berhasil!',
            message: "{{ session('success') }}",
            position: 'topRight'
        });
        @endif


        // Validation on form
        $('body').on('keyup', '#name', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('keyup', '#agency', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('keyup', '#phone', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('keyup', '#email', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('keyup', '#date', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('change', '#description', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
    });
</script>


@endpush