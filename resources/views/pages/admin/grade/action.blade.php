<div class="form-button-action">
    <button data-toggle="tooltip" id="btn-edit" class="btn btn-sm btn-icon btn-warning" value="{{ $id }}">
        <i class="fa fa-edit"></i>
    </button>
    <button data-toggle="tooltip" id="btn-delete" class="btn btn-sm btn-icon btn-danger" value="{{ $id }}">
        <i class="fa fa-trash-alt"></i>
    </button>
</div>