@extends('layouts.app')

@section('title', 'Data Pangkat')

@push('style')
<!-- CSS Libraries -->
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.min.css">

@endpush

@section('main')

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="grade-form">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="name">Nama Pangkat <sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan nama pangkat..." autocomplete="off">
                        <div class="invalid-feedback" id="valid-name"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer no-bd">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fas fa-times"></i>
                    Close
                </button>
                <button type="button" id="btn-save" class="btn btn-primary">
                    <i class="fas fa-check"></i>
                    Simpan
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Main -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Pangkat</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Data Pangkat</div>
            </div>
        </div>

        <div class="section-body">
            <div class="card card-primary">
                <div class="card-header">
                    <button class="btn btn-primary ml-auto" id="btn-add">
                        <i class="fas fa-plus-circle"></i>
                        Tambah Data
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped w-100" id="table-grade">
                            <thead>
                                <tr>
                                    <th width="50px">No</th>
                                    <th>Nama Pangkat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@push('scripts')
<!-- JS Libraies -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.all.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // Setup AJAX CSRF
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Initializing DataTable
        $('#table-grade').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ route('admin.grades') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'name',
                    name: 'name',
                },
                {
                    data: 'action',
                    name: 'action',
                    className: 'text-center',
                    orderable: false,
                    searchable: false
                },
            ]
        });

        // Open Modal
        $('#btn-add').click(function() {
            $('#formModal').modal('show');
            $('.modal-title').html('Tambah Data');
            $('#grade-form').trigger('reset');
            $('#btn-save').html('<i class="fas fa-check"></i> Simpan');
            $('#grade-form').find('.form-control').removeClass('is-invalid is-valid');
            $('#btn-save').val('save').removeAttr('disabled');
        });

        // Store or Update
        $('#btn-save').click(function() {
            var formData = {
                name: $('#name').val(),
            };
            var state = $('#btn-save').val();
            var type = "POST";
            var ajaxurl = "{{ route('admin.grades.store') }}";
            $('#btn-save').html('<i class="fas fa-cog fa-spin"></i> Saving...').attr("disabled", true);
            if (state == "update") {
                $('#btn-save').html('<i class="fas fa-cog fa-spin"></i> Updating...').attr("disabled", true);
                var id = $('#id').val();
                type = "PUT";
                ajaxurl = "{{ route('admin.grades') }}" + '/update/' + id;
            }
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function(data) {
                    if (state == "save") {
                        swal.fire("Berhasil!", 'Data berhasil ditambahkan', "success");
                        $('#table-grade').DataTable().draw(false);
                        $('#table-grade').DataTable().on('draw', function() {
                            $('[data-toggle="tooltip"]').tooltip();
                        });
                    } else {
                        swal.fire("Berhasil!", 'Data berhasil di ubah', "success");
                        $('#table-grade').DataTable().draw(false);
                        $('#table-grade').DataTable().on('draw', function() {
                            $('[data-toggle="tooltip"]').tooltip();
                        });
                    }
                    $('#formModal').modal('hide');
                },
                error: function(data) {
                    try {
                        if (state == "save") {
                            if (data.responseJSON.errors.name) {
                                $('#name').removeClass('is-valid').addClass('is-invalid');
                                $('#valid-name').removeClass('valid-feedback').addClass('invalid-feedback');
                                $('#valid-name').html(data.responseJSON.errors.name);
                            }
                            $('#btn-save').html('<i class="fas fa-check"></i> Simpan');
                            $('#btn-save').removeAttr('disabled');
                        } else {
                            if (data.responseJSON.errors.name) {
                                $('#name').removeClass('is-valid').addClass('is-invalid');
                                $('#valid-name').removeClass('valid-feedback').addClass('invalid-feedback');
                                $('#valid-name').html(data.responseJSON.errors.name);
                            }
                            $('#btn-save').html('<i class="fas fa-check"></i> Update');
                            $('#btn-save').removeAttr('disabled');
                        }
                    } catch {
                        swal.fire("Maaf!", 'Terjadi kesalahan, Silahkan coba lagi', "error");
                        $('#formModal').modal('hide');
                    }
                }
            });
        });

        // Edit
        $('body').on('click', '#btn-edit', function() {
            var id = $(this).val();
            var name = $(this).data('name');
            $.get("{{ route('admin.grades') }}" + '/edit/' + id, function(data) {
                $('#grade-form').find('.form-control').removeClass('is-invalid is-valid');
                $('#id').val(data.id);
                $('#name').val(data.name);
                $('#btn-save').val('update').removeAttr('disabled');
                $('#formModal').modal('show');
                $('.modal-title').html('Edit Data');
                $('#btn-save').html('<i class="fas fa-check"></i> Edit');
            }).fail(function() {
                swal.fire("Maaf!", 'Gagal mengambil Data', "error");
            });
        });

        // Delete
        $('body').on('click', '#btn-delete', function() {
            var id = $(this).val();
            var name = $(this).data('name');

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-lg btn-primary mr-2',
                    cancelButton: 'btn btn-lg btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Peringatan!',
                text: "Apakah anda yakin?",
                imageWidth: 100,
                imageHeight: 100,
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "DELETE",
                        url: "{{ route('admin.grades') }}" + '/destroy/' + id,
                        success: function(data) {
                            $('#table-grade').DataTable().draw(false);
                            $('#table-grade').DataTable().on('draw', function() {
                                $('[data-toggle="tooltip"]').tooltip();
                            });
                            swal.fire("Berhasil!", 'Data berhasil dihapus', "success");
                        },
                        error: function(data) {
                            swal.fire("Maaf!", 'Terjadi kesalahan, Silahkan coba lagi', "error");
                        }
                    });
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swal.fire("Oh Ya!", 'Data aman, jangan khawatir', "error")
                }
            })


        });

        // Validation on form
        $('body').on('keyup', '#name', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
    });
</script>
@endpush