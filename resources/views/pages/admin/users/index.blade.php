@extends('layouts.app')

@section('title', 'Data User')

@push('style')
<!-- CSS Libraries -->
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.min.css">

@endpush

@section('main')
<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="user-form">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="employment_number">NIP <sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="employment_number" name="employment_number" placeholder="Masukkan NIP..." autocomplete="off" autofocus>
                        <div class="invalid-feedback" id="valid-employment_number"></div>
                    </div>
                    <div class="form-group">
                        <label for="name">Nama Lengkap<sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan nama lengkap..." autocomplete="off">
                        <div class="invalid-feedback" id="valid-name"></div>
                    </div>
                    <div class="form-group">
                        <label for="position">Nama Jabatan<sup class="text-danger">*</sup></label>
                        <input type="text" class="form-control" id="position" name="position" placeholder="Masukkan nama jabatan..." autocomplete="off">
                        <div class="invalid-feedback" id="valid-position"></div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email <sup class="text-danger">*</sup></label>
                        <input type="email" class="form-control" id="email" name="email" autocomplete="off" placeholder="Masukkan Email...">
                        <div class="invalid-feedback" id="valid-email"></div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password <sup class="text-danger">*</sup></label>
                        <input type="password" class="form-control" id="password" name="password" autocomplete="off" placeholder="Masukkan Password...">
                        <small id="null"></small>
                        <div class="invalid-feedback" id="valid-password"></div>
                    </div>
                    <div class="form-group">
                        <label for="grade_id">Pangkat / Golongan</label>
                        <select class="select2 form-control form-control-sm @error('grade_id') is-invalid @enderror" name="grade_id" id="grade_id">
                            <option value="" selected disabled>-- Pilih Pangkat / Golongan --</option>
                            @foreach ($grades as $grade)
                            <option value="{{ $grade->id }}" {{ old('grade_id') == $grade->name ? 'selected' : '' }}>{{ $grade->name }}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback" id="valid-grade_id">{{ $errors->first('grade_id') }}</div>
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>
                        <select class="select2 form-control form-control-sm @error('role') is-invalid @enderror" name="role" id="role">
                            <option value="" selected disabled>-- Pilih Role --</option>
                            <option value="kabalai">Kabalai</option>
                            <option value="kabag">Kabar/Kasie/Koordinator</option>
                            <option value="staff">Pengarsipan/Tata Usaha</option>
                        </select>
                        <div class="invalid-feedback" id="valid-role">{{ $errors->first('role') }}</div>
                    </div>
                </form>

            </div>
            <div class="modal-footer no-bd">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <i class="fas fa-times"></i>
                    Close
                </button>
                <button type="button" id="btn-save" class="btn btn-primary">
                    <i class="fas fa-check"></i>
                    Save Changes
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Main -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data User</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Data User</div>
            </div>
        </div>

        <div class="section-body">
            <div class="card card-primary">
                <div class="card-header">
                    <button class="btn btn-primary ml-auto" id="btn-add">
                        <i class="fas fa-plus-circle"></i>
                        Tambah Data
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped w-100" id="table-user">
                            <thead>
                                <tr>
                                    <th width="30px">No</th>
                                    <th>NIP</th>
                                    <th>Nama Lengkap</th>
                                    <th>Jabatan</th>
                                    <th>Email</th>
                                    <th>Pangkat</th>
                                    <th>Role</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('scripts')
<!-- JS Libraies -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.all.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // Setup AJAX CSRF
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Initializing DataTable
        $('#table-user').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ route('admin.users') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'employment_number',
                    name: 'employment_number',
                },
                {
                    data: 'name',
                    name: 'name',
                },
                {
                    data: 'position',
                    name: 'position',
                },
                {
                    data: 'email',
                    name: 'email',
                },
                {
                    data: 'grade',
                    name: 'grade',
                },
                {
                    data: 'role',
                    name: 'role',
                },
                {
                    data: 'action',
                    name: 'action',
                    className: 'text-center',
                    orderable: false,
                    searchable: false
                },
            ]
        });

        // Open Modal to Add new Users
        $('#btn-add').click(function() {
            $('#formModal').modal('show');
            $('.modal-title').html('Tambah Data');
            $('#user-form').trigger('reset');
            $('#btn-save').html('<i class="fas fa-check"></i> Simpan');
            $('#user-form').find('.form-control').removeClass('is-invalid is-valid');
            $('#btn-save').val('save').removeAttr('disabled');
        });

        // Store new grade or update grade
        $('#btn-save').click(function() {
            var formData = {
                employment_number: $('#employment_number').val(),
                name: $('#name').val(),
                position: $('#position').val(),
                email: $('#email').val(),
                password: $('#password').val(),
                grade_id: $('#grade_id').val(),
                role: $('#role').val(),
            };
            var state = $('#btn-save').val();
            var type = "POST";
            var ajaxurl = "{{ route('admin.users.store') }}";
            $('#btn-save').html('<i class="fas fa-cog fa-spin"></i> Saving...').attr("disabled", true);
            if (state == "update") {
                $('#btn-save').html('<i class="fas fa-cog fa-spin"></i> Updating...').attr("disabled", true);
                var id = $('#id').val();
                type = "PUT";
                ajaxurl = "{{ route('admin.users') }}" + '/update/' + id;
            }
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function(data) {
                    if (state == "save") {
                        swal.fire("Berhasil!", 'Data berhasil ditambahkan', "success");
                        $('#table-user').DataTable().draw(false);
                        $('#table-user').DataTable().on('draw', function() {
                            $('[data-toggle="tooltip"]').tooltip();
                        });
                    } else {
                        swal.fire("Berhasil!", 'Data berhasil di ubah', "success");
                        $('#table-user').DataTable().draw(false);
                        $('#table-user').DataTable().on('draw', function() {
                            $('[data-toggle="tooltip"]').tooltip();
                        });
                    }
                    $('#formModal').modal('hide');
                },
                error: function(data) {
                    try {
                        if (data.responseJSON.errors.employment_number) {
                            $('#employment_number').removeClass('is-valid').addClass('is-invalid');
                            $('#valid-employment_number').removeClass('valid-feedback').addClass('invalid-feedback');
                            $('#valid-employment_number').html(data.responseJSON.errors.employment_number);
                        }
                        if (data.responseJSON.errors.name) {
                            $('#name').removeClass('is-valid').addClass('is-invalid');
                            $('#valid-name').removeClass('valid-feedback').addClass('invalid-feedback');
                            $('#valid-name').html(data.responseJSON.errors.name);
                        }
                        if (data.responseJSON.errors.position) {
                            $('#position').removeClass('is-valid').addClass('is-invalid');
                            $('#valid-position').removeClass('valid-feedback').addClass('invalid-feedback');
                            $('#valid-position').html(data.responseJSON.errors.position);
                        }
                        if (data.responseJSON.errors.email) {
                            $('#email').removeClass('is-valid').addClass('is-invalid');
                            $('#valid-email').removeClass('valid-feedback').addClass('invalid-feedback');
                            $('#valid-email').html(data.responseJSON.errors.email);
                        }
                        if (data.responseJSON.errors.password) {
                            $('#password').removeClass('is-valid').addClass('is-invalid');
                            $('#valid-password').removeClass('valid-feedback').addClass('invalid-feedback');
                            $('#valid-password').html(data.responseJSON.errors.password);
                        }
                        if (data.responseJSON.errors.grade_id) {
                            $('#grade_id').removeClass('is-valid').addClass('is-invalid');
                            $('#valid-grade_id').removeClass('valid-feedback').addClass('invalid-feedback');
                            $('#valid-grade_id').html(data.responseJSON.errors.grade_id);
                        }
                        if (data.responseJSON.errors.role) {
                            $('#role').removeClass('is-valid').addClass('is-invalid');
                            $('#valid-role').removeClass('valid-feedback').addClass('invalid-feedback');
                            $('#valid-role').html(data.responseJSON.errors.role);
                        }
                        if (state == "save") {
                            $('#btn-save').html('<i class="fas fa-check"></i> Simpan');
                            $('#btn-save').removeAttr('disabled');
                        } else {
                            $('#btn-save').html('<i class="fas fa-check"></i> Update');
                            $('#btn-save').removeAttr('disabled');
                        }
                    } catch {
                        swal.fire("Maaf!", 'Terjadi kesalahan, Silahkan coba lagi', "error");
                        $('#formModal').modal('hide');
                    }
                }
            });
        });

        // Edit Grade
        $('body').on('click', '#btn-edit', function() {
            var id = $(this).val();
            var name = $(this).data('name');
            $.get("{{ route('admin.users') }}" + '/edit/' + id, function(data) {
                $('#user-form').find('.form-control').removeClass('is-invalid is-valid');
                $('#id').val(data.id);
                $('#employment_number').val(data.employment_number);
                $('#name').val(data.name);
                $('#position').val(data.position);
                $('#email').val(data.email);
                $('#password').val(data.password);
                $('#grade_id').val(data.grade_id);
                $('#role').val(data.role);
                $('#btn-save').val('update').removeAttr('disabled');
                $('#formModal').modal('show');
                $('.modal-title').html('Edit Data');
                $('#null').html('<small id="null">Kosongkan jika tidak ingin di ubah</small>');
                $('#btn-save').html('<i class="fas fa-check"></i> Edit');
            }).fail(function() {
                swal.fire("Maaf!", 'Gagal mengambil Data', "error");
            });
        });

        // Delete
        $('body').on('click', '#btn-delete', function() {
            var id = $(this).val();

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-lg btn-primary mr-2',
                    cancelButton: 'btn btn-lg btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Peringatan!',
                text: "Apakah anda yakin?",
                imageWidth: 100,
                imageHeight: 100,
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "DELETE",
                        url: "{{ route('admin.users') }}" + '/destroy/' + id,
                        success: function(data) {
                            $('#table-user').DataTable().draw(false);
                            $('#table-user').DataTable().on('draw', function() {
                                $('[data-toggle="tooltip"]').tooltip();
                            });
                            swal.fire("Berhasil!", 'Data berhasil dihapus', "success");
                        },
                        error: function(data) {
                            swal.fire("Maaf!", 'Terjadi kesalahan, Silahkan coba lagi', "error");
                        }
                    });
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swal.fire("Oh Ya!", 'Data aman, jangan khawatir', "error")
                }
            })


        });

        // Validation on form
        $('body').on('keyup', '#employment_number', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('keyup', '#name', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('keyup', '#position', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('keyup', '#email', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('keyup', '#password', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('change', '#grade_id', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
        $('body').on('change', '#role', function() {
            var test = $(this).val();
            if (test == '') {
                $(this).removeClass('is-valid is-invalid');
            } else {
                $(this).removeClass('is-invalid').addClass('is-valid');
            }
        });
    });
</script>
@endpush