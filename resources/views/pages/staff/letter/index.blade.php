@extends('layouts.app')

@section('title', 'Surat Masuk')

@push('style')
<!-- CSS Libraries -->
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.min.css">

@endpush

@section('main')
<!-- Main -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Surat Masuk</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">Surat Masuk</div>
            </div>
        </div>

        @if (session('success'))
        <div class="alert alert-primary alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>
                {!! session('success') !!}
            </div>
        </div>
        @endif

        <div class="section-body">
            <div class="card card-primary">
                <div class="card-header">
                    <div class="row">
                        <input type="date" class="form-control form-control-sm w-25 mr-2" name="from" id="from">
                        <input type="date" class="form-control form-control-sm w-25 mr-2" name="to" id="to">
                        <button class="btn btn-primary mr-2" id="btn-filter">
                            Filter
                        </button>
                        <button class="btn btn-danger" id="btn-reset">
                            Reset
                        </button>
                        <button class="btn btn-primary ml-auto" id="btn-print">
                            <i class="fas fa-print"></i>
                            Print
                        </button>
                    </div>
                    <a href="{{ route('staff.letter.create')}}" class="btn btn-primary ml-auto">
                        <i class="fas fa-plus-circle"></i>
                        Tambah Data
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped w-100" id="table-letter">
                            <thead>
                                <tr>
                                    <th width="30px">No</th>
                                    <th>No. Surat Masuk</th>
                                    <th>Asal Instansi</th>
                                    <th>Tanggal Masuk</th>
                                    <th>Keterangan</th>
                                    <th>Dokumen</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('scripts')
<!-- JS Libraies -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.5.1/sweetalert2.all.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // Setup AJAX CSRF
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        load_data();

        function load_data(from_date = '', to_date = '') {
            $('#table-letter').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: {
                    url: "{{ route('staff.letter') }}",
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'letter_number',
                        name: 'letter_number',
                    },
                    {
                        data: 'sender',
                        name: 'sender',
                    },
                    {
                        data: 'date',
                        name: 'date',
                    },
                    {
                        data: 'description',
                        name: 'description',
                    },
                    {
                        data: 'document',
                        name: 'document',
                    },
                    {
                        data: 'action',
                        name: 'action',
                        className: 'text-center',
                        orderable: false,
                        searchable: false
                    },
                ],
            });
        }

        // Delete
        $('body').on('click', '#btn-delete', function() {
            var id = $(this).val();

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-lg btn-primary mr-2',
                    cancelButton: 'btn btn-lg btn-secondary'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Peringatan!',
                text: "Apakah anda yakin?",
                imageWidth: 100,
                imageHeight: 100,
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "DELETE",
                        url: "{{ route('staff.letter') }}" + '/destroy/' + id,
                        success: function(data) {
                            $('#table-letter').DataTable().draw(false);
                            $('#table-letter').DataTable().on('draw', function() {
                                $('[data-toggle="tooltip"]').tooltip();
                            });
                            swal.fire("Berhasil!", 'Data berhasil dihapus', "success");
                        },
                        error: function(data) {
                            swal.fire("Maaf!", 'Terjadi kesalahan, Silahkan coba lagi', "error");
                        }
                    });
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swal.fire("Oh Ya!", 'Data aman, jangan khawatir', "error")
                }
            })
        });

        $('#btn-filter').click(function() {
            var from_date = $('#from').val();
            var to_date = $('#to').val();
            if (from_date != '' && to_date != '') {
                $('#table-letter').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#btn-reset').click(function() {
            $('#from').val('');
            $('#to').val('');
            $('#table-letter').DataTable().destroy();
            load_data();
        });

        $('#btn-print').click(function() {
            var formData = {
                from_date: $('#from').val(),
                to_date: $('#to').val(),
            };

            $.ajax({
                type: "GET",
                url: "{{ route('staff.letter.print') }}",
                data: formData,
                xhrFields: {
                    responseType: 'blob'
                },
                success: function(response) {
                    var blob = new Blob([response]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "Laporan.pdf";
                    link.click();
                },
                error: function(blob) {
                    console.log(blob);
                }
            });
        });
    });
</script>
@endpush