@extends('layouts.app')

@section('title', 'Surat Masuk')

@push('style')

@endpush

@section('main')

<!-- Main -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Upload Surat Masuk</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item">
                    <a href="">
                        Dashboard
                    </a>
                </div>
                <div class="breadcrumb-item">
                    <a href="">
                        Surat Masuk
                    </a>
                </div>
                <div class="breadcrumb-item">
                    Upload
                </div>
            </div>
        </div>
        <div class="section-body">
            <form method="POST" action="{{ route('staff.letter.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-lg-9 col-md-6 col-sm-6 col-12">
                        <div class="card card-primary">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="letter_number">Nomor Surat Masuk <sup class="text-danger">*</sup></label>
                                            <input type="text" class="form-control form-control-sm @error('letter_number') is-invalid @enderror" name="letter_number" id="letter_number" value="{{ old('letter_number') }}" placeholder="Masukkan nomor surat masuk">
                                            <div class="invalid-feedback" id="valid-letter_number">{{ $errors->first('letter_number') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="sender">Asal Instansi <sup class="text-danger">*</sup></label>
                                            <input type="text" class="form-control form-control-sm @error('sender') is-invalid @enderror" name="sender" id="sender" value="{{ old('sender') }}" placeholder="Masukkan asal instansi">
                                            <div class="invalid-feedback" id="valid-sender">{{ $errors->first('sender') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="date">Tanggal Surat Masuk <sup class="text-danger">*</sup></label>
                                            <input type="date" class="form-control form-control-sm @error('date') is-invalid @enderror" name="date" id="date" value="<?= date('Y-m-d'); ?>">
                                            <div class="invalid-feedback" id="valid-date">{{ $errors->first('date') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="document">Doc/File Surat Masuk<sup class="text-danger">max : 2MB</sup></label>
                                            <input type="file" class="form-control form-control-sm @error('document') is-invalid @enderror" name="document" id="document" value="{{ old('document') }}">
                                            <div class="invalid-feedback" id="valid-document">{{ $errors->first('document') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="description">Keterangan <sup class="text-danger">*</sup></label>
                                            <textarea class="form-control form-control-sm @error('description') is-invalid @enderror" name="description" id="description" value="{{ old('description') }}"></textarea>
                                            <div class="invalid-feedback" id="valid-description">{{ $errors->first('description') }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-round float-right" id="btn-submit">
                                            <i class="fas fa-check"></i>
                                            Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@push('scripts')

<script type="text/javascript">
    $(document).ready(function() {

    });
</script>
@endpush