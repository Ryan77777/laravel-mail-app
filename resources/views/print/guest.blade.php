<!DOCTYPE html>
<html>

<head>
    <title>Laporan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        @font-face {
            font-family: 'Poppins';
            src: url("https://fonts.googleapis.com/css2?family=Poppins&display=swap") format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: 'Poppins', sans-serif;
        }

        .styled-table {
            table-layout: fixed;
            border-collapse: collapse;
            margin: 25px 0;
            font-size: 0.7em;
            font-family: 'Poppins', sans-serif;
            min-width: '100%';
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
        }

        .styled-table thead tr {
            background-color: #007bff;
            color: #ffffff;
            text-align: left;
        }

        .styled-table th,
        .styled-table td {
            padding: 12px 15px;
        }

        .styled-table tbody tr {
            border-bottom: 1px solid #dddddd;
        }

        .styled-table tbody tr:nth-of-type(even) {
            background-color: #f3f3f3;
        }

        .styled-table tbody tr:last-of-type {
            border-bottom: 2px solid #007bff;
        }

        td:nth-child(1) {
            width: 5%;
        }

        td:nth-child(2) {
            width: 15%;
            word-wrap: break-word
        }

        td:nth-child(3) {
            width: 15%;
            word-wrap: break-word
        }

        td:nth-child(4) {
            width: 13%;
        }

        td:nth-child(5) {
            width: 20%;
            word-wrap: break-word
        }

        td:nth-child(6) {
            width: 15%;
        }

        td:nth-child(7) {
            width: 17%;
            word-wrap: break-word
        }
    </style>
</head>

<body>

    <!-- Header -->
    <div class="kop" style="font-family: 'Poppins', sans-serif;width: 100%;height: 120px;display: flex;align-items: center;justify-content: center;">
        <table style="width: 100%;margin-left: 60px;">
            <tr>
                <td style="width: 52px;height: 48px;">
                    <img src="./logo.jpeg" alt="Logo Pemprov" style="width: 48px;height: 48px;" class="logo-kop" />
                </td>
                <td>
                    <p class="text-kop" style="font-size: 14px;line-height: 20px;color: black;margin-left: 12px;width: 100%;position: relative;top: 6px;font-weight: 400;">
                        BALAI TEKNOLOGI KOMUNIKASI DAN INFORMASI PENDIDIKAN
                        DAN
                        <br>
                        KEBUDAYAAN
                        <br>
                        PEMERINTAH PROVINSI MALUKU
                    </p>
                </td>
            </tr>
        </table>
    </div>


    <!-- Title -->
    <table style="width: 100%">
        <tr>
            <td style="width: 100%;text-align: center;">
                <h1 style="font-size: 16px; font-family: 'Poppins', sans-serif; font-weight: 400;">{{ $title }}</h1>
            </td>
        </tr>
    </table>

    <!-- Table -->
    <table class="table styled-table">
        <?php
        $no = 1;
        ?>
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Tamu</th>
                <th>Asal Instansi</th>
                <th>No. Hp</th>
                <th>Email</th>
                <th>Tanggal</th>
                <th>Keperluan</th>
            </tr>
        </thead>
        <tbody>
            @if(count($guests) > 0)
            @foreach($guests as $guest)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $guest->name }}</td>
                <td>{{ $guest->agency }}</td>
                <td>{{ $guest->phone }}</td>
                <td>{{ $guest->email }}</td>
                <td>{{ $guest->date }}</td>
                <td>{{ $guest->description }}</td>
            </tr>
            @endforeach
            @else
            <tr style="text-align: center;">
                <td colspan="7">Data tidak ditemukan</td>
            </tr>
            @endif
        </tbody>
    </table>
</body>

</html>