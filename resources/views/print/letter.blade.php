<!DOCTYPE html>
<html>

<head>
    <title>Laporan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        @font-face {
            font-family: 'Poppins';
            src: url("https://fonts.googleapis.com/css2?family=Poppins&display=swap") format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: 'Poppins', sans-serif;
        }

        .styled-table {
            border-collapse: collapse;
            margin: 25px 0;
            font-size: 0.7em;
            font-family: 'Poppins', sans-serif;
            min-width: '100%';
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
        }

        .styled-table thead tr {
            background-color: #007bff;
            color: #ffffff;
            text-align: left;
        }

        .styled-table th,
        .styled-table td {
            padding: 12px 15px;
        }

        .styled-table tbody tr {
            border-bottom: 1px solid #dddddd;
        }

        .styled-table tbody tr:nth-of-type(even) {
            background-color: #f3f3f3;
        }

        .styled-table tbody tr:last-of-type {
            border-bottom: 2px solid #007bff;
        }
    </style>
</head>

<body>

    <!-- Header -->
    <div class="kop" style="font-family: 'Poppins', sans-serif;width: 100%;height: 120px;display: flex;align-items: center;justify-content: center;">
        <table style="width: 100%;margin-left: 60px;">
            <tr>
                <td style="width: 52px;height: 48px;">
                    <img src="./logo.jpeg" alt="Logo Pemprov" style="width: 48px;height: 48px;" class="logo-kop" />
                </td>
                <td>
                    <p class="text-kop" style="font-size: 14px;line-height: 20px;color: black;margin-left: 12px;width: 100%;position: relative;top: 6px;font-weight: 400">
                        BALAI TEKNOLOGI KOMUNIKASI DAN INFORMASI PENDIDIKAN
                        DAN
                        <br>
                        KEBUDAYAAN
                        <br>
                        PEMERINTAH PROVINSI MALUKU
                    </p>
                </td>
            </tr>
        </table>
    </div>


    <!-- Title -->
    <table style="width: 100%">
        <tr>
            <td style="width: 100%;text-align: center;">
                <h1 style="font-size: 16px; font-family: 'Poppins', sans-serif; font-weight: 400;">{{ $title }}</h1>
            </td>
        </tr>
    </table>

    <!-- Table -->
    <table class="table styled-table">
        <?php
        $no = 1;
        ?>
        <thead>
            <tr>
                <th>No</th>
                <th>No. Surat Masuk</th>
                <th>Instansi</th>
                <th>Tanggal Masuk</th>
            </tr>
        </thead>
        <tbody>
            @if(count($letters) > 0)
            @foreach($letters as $letter)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $letter->letter_number }}</td>
                <td>{{ $letter->sender }}</td>
                <td>{{ $letter->date }}</td>
            </tr>
            @endforeach
            @else
            <tr style="text-align: center;">
                <td colspan="4">Data tidak ditemukan</td>
            </tr>
            @endif
        </tbody>
    </table>
</body>

</html>